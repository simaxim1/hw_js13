/* 
  1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
        setTimeout() - одноразова дія коду в середені функції, setInterval(`) використовується для багаторазового повторювання дій до зупинки/видалення;

  2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку ? Чи спрацює вона миттєво і чому ?
        функція спрацює, але тільки у черзі подій браузера, які виконуються до неї. Краще використовувати дефолтне значення у 4мс.

  3. Чому важливо не забувати викликати функцію`clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен ?
        Це запобігає використанню додаткових ресурсів та генеруванню непередбачуванних результатів взаімодії з іншіми функціями.
*/


const images = document.querySelectorAll('.image-to-show');
const continueBtn = document.querySelector('.continue');
const pauseBtn = document.querySelector('.pause');

let timer = null;
let slide = 0;


const slideShowing = () => {
  images.forEach((e) => {
    e.classList.remove('active');
  });
  
  if (slide >= images.length) {
    slide = 0;
  } images[slide++].classList.add('active');
}

timer = setInterval(slideShowing, 3000);

continueBtn.addEventListener('click', () => {
  timer = setInterval(slideShowing, 3000);
});

pauseBtn.addEventListener('click', () => {
  clearInterval(timer)
});